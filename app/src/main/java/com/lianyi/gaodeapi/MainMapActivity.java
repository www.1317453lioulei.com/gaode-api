package com.lianyi.gaodeapi;

import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.MapView;
import com.amap.api.maps.model.CameraPosition;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.MyLocationStyle;
import com.lianyi.gaodeapi.util.GpsUtil;
import com.lianyi.gaodeapi.util.PermissionXUtil;

public class MainMapActivity extends AppCompatActivity {
    private MapView mMapView;
    private AMap aMap;

    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        mMapView = findViewById(R.id.mMapView);
        mMapView.onCreate(savedInstanceState);
        boolean isOpenGps = GpsUtil.INSTANCE.isOPen(this);
        if (isOpenGps) {
            initPermission();
        } else {
            GpsUtil.INSTANCE.openGps(this, GpsUtil.REQUEST_CODE_OPEN_GPS);
        }

    }

    private void initPermission() {
        PermissionXUtil.INSTANCE.perLocation(this, new PermissionXUtil.PermissionXListener() {
            @Override
            public void onSuccess() {
                initMap();
            }
        });
    }

    private void initMap() {
        aMap = mMapView.getMap();
        MyLocationStyle myLocationStyle = new MyLocationStyle();
        myLocationStyle.myLocationType(MyLocationStyle.LOCATION_TYPE_LOCATE);
        //myLocationStyle.interval(2000) //设置连续定位模式下的定位间隔，只在连续定位模式下生效，单次定位模式下不会生效。单位为毫秒。
        myLocationStyle.strokeColor(Color.TRANSPARENT);// 设置圆形的边框颜色
        myLocationStyle.radiusFillColor(Color.argb(0, 0, 0, 0));// 设置圆形的填充颜色
        myLocationStyle.strokeWidth(1.0f);// 设置圆形的边框粗细
        aMap.setMyLocationStyle(myLocationStyle);  //设置定位蓝点的Style
        aMap.setMyLocationEnabled(true); // 设置为true表示启动显示定位蓝点，false表示隐藏定位蓝点并不进行定位，默认是false。
        aMap.getUiSettings().setMyLocationButtonEnabled(true);
//        aMap.setMinZoomLevel(20);//不让用户缩放地图
//        aMap.moveCamera(CameraUpdateFactory.zoomTo(20));
        aMap.addOnMyLocationChangeListener(new AMap.OnMyLocationChangeListener() {
            @Override
            public void onMyLocationChange(Location location) {
                double lat = location.getLatitude();
                double lon = location.getLongitude();
                Log.i("asd", lat + "  " + lon);
            }
        });
        aMap.moveCamera(
                CameraUpdateFactory.newCameraPosition(
                        new CameraPosition(
                                new LatLng(
                                        0.0,
                                        0.0
                                ), 15f, 0f, 0f
                        )
                )
        );
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //在activity执行onPause时执行mMapView.onPause ()，暂停地图的绘制
        mMapView.onPause();
    }
}
