package com.lianyi.gaodeapi.dialog

import android.app.Dialog
import android.content.Context
import android.util.DisplayMetrics
import android.view.Window
import android.view.WindowManager
import com.lianyi.gaodeapi.R

abstract class BaseDialog(context: Context, var width: Float = 0.65f, height: Float = -1.0f) : Dialog(context, R.style.public_Dialog) {
    private var mHeight = height

    override fun show() {
        super.show()
        setWindowSize()
        showBaseDialog()
    }


    abstract fun showBaseDialog()

    private fun setWindowSize() {
        val dm = DisplayMetrics()
        val m: WindowManager = window!!.windowManager
        m.defaultDisplay.getMetrics(dm)

        // 为获取屏幕宽、高
        val p = window!!.attributes // 获取对话框当前的參数值
        if (mHeight != -1.0f) {
            p.height = ((dm.heightPixels) * mHeight).toInt() //高度设置为屏幕的1.0
        }
        p.width = ((dm.widthPixels * width).toInt()) // 宽度设置为屏幕的0.65
        p.alpha = 1.0f // 设置本身透明度
        p.dimAmount = 0.6f // 设置黑暗度
        window!!.attributes = p
    }
}