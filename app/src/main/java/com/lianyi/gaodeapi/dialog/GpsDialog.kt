package com.lianyi.gaodeapi.dialog

import android.content.Context
import android.os.Bundle
import com.lianyi.gaodeapi.R
import kotlinx.android.synthetic.main.public_gps_dialog_layout.*

class GpsDialog(mContent: Context, listener: onGpsClickListener) : BaseDialog(mContent) {
    val listener = listener
    override fun showBaseDialog() {

    }

    companion object {
        const val CANCELED_ONT_OUCH_OUTSIDE = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.public_gps_dialog_layout)
        setCanceledOnTouchOutside(CANCELED_ONT_OUCH_OUTSIDE)
        initListener()
    }

    fun initListener() {
        tv_btn_sure.setOnClickListener {
            this.dismiss()
            listener.confirm()
        }
        tv_btn_dissmiss.setOnClickListener {
            this.dismiss()
            listener.cancel()
        }

    }


    interface onGpsClickListener {
        fun confirm()
        fun cancel() = Unit
    }

}