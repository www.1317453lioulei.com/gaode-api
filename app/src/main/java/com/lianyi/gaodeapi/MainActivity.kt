package com.lianyi.gaodeapi

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.lianyi.gaodeapi.util.PermissionXUtil.PermissionXListener
import com.lianyi.gaodeapi.util.PermissionXUtil.perLocation
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        map?.setOnClickListener {
            initPermission()
        }
    }

    fun initPermission() {
        perLocation(this, object : PermissionXListener {
            override fun onSuccess() {
                startActivity(Intent(this@MainActivity, MapActivity::class.java))
            }
        })
    }
}