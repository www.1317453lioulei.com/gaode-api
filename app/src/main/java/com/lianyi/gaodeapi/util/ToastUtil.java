package com.lianyi.gaodeapi.util;


import android.widget.Toast;

import com.lianyi.gaodeapi.MyApp;


public class ToastUtil {
    private static Toast mToast;
    private static Toast toast2;

    /**
     * 解决系统Toast连续点击重复显示问题
     *
     * @param msg
     */
    public static void showShortToast(String msg) {
        try {
            if (mToast != null) {
                mToast.cancel();
                mToast = null;
            }
            mToast = ToastCompat.makeText(MyApp.Companion.getContext(), msg, Toast.LENGTH_SHORT);
            mToast.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showLongToast(String msg) {
        try {
            if (mToast != null) {
                mToast.cancel();
                mToast = null;
            }
            mToast = ToastCompat.makeText(MyApp.Companion.getContext(), msg, Toast.LENGTH_LONG);
            mToast.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
