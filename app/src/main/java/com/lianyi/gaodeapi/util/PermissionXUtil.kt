package com.lianyi.gaodeapi.util

import android.Manifest
import android.app.Activity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.permissionx.guolindev.PermissionCollection
import com.permissionx.guolindev.callback.ExplainReasonCallbackWithBeforeParam
import com.permissionx.guolindev.callback.ForwardToSettingsCallback
import com.permissionx.guolindev.callback.RequestCallback

object PermissionXUtil {

    /**
     * 相机和读写权限
     */
    fun perReadAndCamera(context: Any, listener: PermissionXListener) {
        var mPermissionCollection: PermissionCollection? = null
        when (context) {
            is Activity -> {
                mPermissionCollection = PermissionCollection(context as FragmentActivity)
            }
            is Fragment -> {
                mPermissionCollection = PermissionCollection(context)
            }
            else -> {
                ToastUtil.showShortToast("由于PermissionX框架限制，context请传入Fragment or Activity，现在你的代码并没有继续执行")
            }
        }
        mPermissionCollection?.permissions(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA
        )
                ?.explainReasonBeforeRequest()
                ?.onExplainRequestReason(ExplainReasonCallbackWithBeforeParam() { scope, deniedList, beforeRequest ->
                    scope.showRequestReasonDialog(deniedList, "应用需要以下权限才能使用", "确定")
                })
                ?.onForwardToSettings(ForwardToSettingsCallback { scope, deniedList ->
                    scope.showForwardToSettingsDialog(
                            deniedList,
                            "请在设置权限管理中允许以下权限",
                            "确定"
                    )
                })
                ?.request(RequestCallback { allGranted, grantedList, deniedList ->
                    if (allGranted) {
                        listener.onSuccess()
                    } else {
                        ToastUtil.showShortToast("您拒绝了如下权限：$deniedList")
                    }
                })
    }

    /**
     * 读写权限
     */
    fun perReadAndWrite(context: Any, listener: PermissionXListener) {
        var mPermissionCollection: PermissionCollection? = null
        when (context) {
            is Activity -> {
                mPermissionCollection = PermissionCollection(context as FragmentActivity)
            }
            is Fragment -> {
                mPermissionCollection = PermissionCollection(context)
            }
            else -> {
                ToastUtil.showShortToast("由于PermissionX框架限制，context请传入Fragment or Activity，现在你的代码并没有继续执行")
            }
        }
        mPermissionCollection?.permissions(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
        )?.explainReasonBeforeRequest()?.onExplainRequestReason(ExplainReasonCallbackWithBeforeParam() { scope, deniedList, beforeRequest ->
            scope.showRequestReasonDialog(deniedList, "应用需要以下权限才能使用", "确定")
        })?.onForwardToSettings(ForwardToSettingsCallback { scope, deniedList ->
            scope.showForwardToSettingsDialog(
                    deniedList,
                    "请在设置权限管理中允许以下权限",
                    "确定"
            )
        })?.request(RequestCallback { allGranted, grantedList, deniedList ->
            if (allGranted) {
                listener.onSuccess()
            } else {
                ToastUtil.showShortToast("您拒绝了如下权限：$deniedList")
            }
        })

    }

    /**
     * 读写权限
     */
    fun perReadAndWriteAndPhone(context: Any, listener: PermissionXListener) {
        var mPermissionCollection: PermissionCollection? = null
        when (context) {
            is Activity -> {
                mPermissionCollection = PermissionCollection(context as FragmentActivity)
            }
            is Fragment -> {
                mPermissionCollection = PermissionCollection(context)
            }
            else -> {
                ToastUtil.showShortToast("由于PermissionX框架限制，context请传入Fragment or Activity，现在你的代码并没有继续执行")
            }
        }
        mPermissionCollection?.permissions(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.READ_PHONE_STATE
        )?.explainReasonBeforeRequest()?.onExplainRequestReason(ExplainReasonCallbackWithBeforeParam() { scope, deniedList, beforeRequest ->
            scope.showRequestReasonDialog(deniedList, "应用需要以下权限才能使用", "确定")
        })?.onForwardToSettings(ForwardToSettingsCallback { scope, deniedList ->
            scope.showForwardToSettingsDialog(
                    deniedList,
                    "请在设置权限管理中允许以下权限",
                    "确定"
            )
        })?.request(RequestCallback { allGranted, grantedList, deniedList ->
            if (allGranted) {
                listener.onSuccess()
            } else {
                ToastUtil.showShortToast("您拒绝了如下权限：$deniedList")
            }
        })

    }



    /**
     * 定位权限
     */

    fun perLocation(context: Any, listener: PermissionXListener) {
        var mPermissionCollection: PermissionCollection? = null
        when (context) {
            is Activity -> {
                mPermissionCollection = PermissionCollection(context as FragmentActivity)
            }
            is Fragment -> {
                mPermissionCollection = PermissionCollection(context)
            }
            else -> {
                ToastUtil.showShortToast("由于PermissionX框架限制，context请传入Fragment or Activity，现在你的代码并没有继续执行")
            }
        }
        mPermissionCollection?.permissions(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
        )
                ?.explainReasonBeforeRequest()
                ?.onExplainRequestReason(ExplainReasonCallbackWithBeforeParam() { scope, deniedList, beforeRequest ->
                    scope.showRequestReasonDialog(deniedList, "应用需要以下权限才能使用", "确定")
                })
                ?.onForwardToSettings(ForwardToSettingsCallback { scope, deniedList ->
                    scope.showForwardToSettingsDialog(
                            deniedList,
                            "请在设置权限管理中允许以下权限",
                            "确定"
                    )
                })
                ?.request(RequestCallback { allGranted, grantedList, deniedList ->
                    if (allGranted) {
                        listener.onSuccess()
                    } else {
                        ToastUtil.showShortToast("您拒绝了如下权限：$deniedList")
                    }
                })

    }
    /**
     * 定位及读写权限
     */
    fun perLocaOrWriteOrRead(context: Any, listener: PermissionXListener) {
        var mPermissionCollection: PermissionCollection? = null
        when (context) {
            is Activity -> {
                mPermissionCollection = PermissionCollection(context as FragmentActivity)
            }
            is Fragment -> {
                mPermissionCollection = PermissionCollection(context)
            }
            else -> {
                ToastUtil.showShortToast("由于PermissionX框架限制，context请传入Fragment or Activity，现在你的代码并没有继续执行")
            }
        }
        mPermissionCollection?.permissions(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
        )
                ?.explainReasonBeforeRequest()
                ?.onExplainRequestReason(ExplainReasonCallbackWithBeforeParam() { scope, deniedList, beforeRequest ->
                    scope.showRequestReasonDialog(deniedList, "应用需要以下权限才能使用", "确定")
                })
                ?.onForwardToSettings(ForwardToSettingsCallback { scope, deniedList ->
                    scope.showForwardToSettingsDialog(
                            deniedList,
                            "请在设置权限管理中允许以下权限",
                            "确定"
                    )
                })
                ?.request(RequestCallback { allGranted, grantedList, deniedList ->
                    if (allGranted) {
                        listener.onSuccess()
                    } else {
                        ToastUtil.showShortToast("您拒绝了如下权限：$deniedList")
                    }
                })

    }


    interface PermissionXListener {
        fun onSuccess()
    }
}