package com.lianyi.gaodeapi

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context

class MyApp : Application() {

    companion object{
        @SuppressLint("StaticFieldLeak")
        lateinit var mContext: Context
        fun getContext(): Context {
            return mContext
        }
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
    }

    override fun onCreate() {
        super.onCreate()
        mContext = this
    }


}